﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63110196_Seminar1.Default" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="color: #000000; background-color: #FFFFFF">
    <form id="form1" runat="server">
        <div style="height: 60px; margin-left: 1200px">
            <asp:Label ID="Label4" runat="server" Text="F1, F4(delno)"></asp:Label>
        </div>
        <hr />
    <div >
    
        <br />
    
        <asp:Label ID="Label1" runat="server" style="font-weight: 700; font-size: x-large; color: #006600" Text="Oddelki in skupine izdelkov"></asp:Label>
        <br />
        <br />
        <asp:DropDownList ID="DDL_Categories" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="ProductCategoryID" style="color: #000000; background-color: #66FF66; margin-top: 0px;" AutoPostBack="True" Height="20px" Width="225px">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID IS NULL)" OnSelecting="SqlDataSource1_Selecting"></asp:SqlDataSource>
        <br />
        <br />
        <asp:GridView ID="GV_Categories" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" GridLines="None" ShowHeader="False" DataKeyNames="ProductCategoryID" SelectedIndex="0" Height="16px" Width="225px">
            <Columns>
                <asp:BoundField DataField="Name" SortExpression="Name" >
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:CommandField ButtonType="Button" SelectText="&gt;" ShowSelectButton="True" >
                <ItemStyle HorizontalAlign="Right" />
                </asp:CommandField>
            </Columns>
            <SelectedRowStyle BackColor="#66FF66" BorderColor="#006600" BorderWidth="3px" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ParentProductCategoryID, ProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Categories" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <hr />
        <br />
        <asp:Label ID="Label2" runat="server" style="font-size: x-large; color: #006600; font-weight: 700" Text="Modeli"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GV_Models" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" GridLines="None" PageSize="5" ShowHeader="False" DataKeyNames="ProductModelID" SelectedIndex="0" Height="220px" Width="1000px">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" >
                <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" >
                <ItemStyle Width="800px" />
                </asp:BoundField>
                <asp:CommandField ShowSelectButton="True" SelectText="Prikaži izdelke" >
                <ItemStyle Width="100px" />
                </asp:CommandField>
            </Columns>
            <SelectedRowStyle BackColor="#66FF66" BorderColor="#006600" BorderWidth="3px" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT SalesLT.ProductModel.Name, SalesLT.ProductDescription.Description, SalesLT.Product.ProductCategoryID, SalesLT.ProductModelProductDescription.Culture, SalesLT.Product.ProductModelID FROM SalesLT.Product INNER JOIN SalesLT.ProductModel ON SalesLT.Product.ProductModelID = SalesLT.ProductModel.ProductModelID INNER JOIN SalesLT.ProductModelProductDescription ON SalesLT.ProductModel.ProductModelID = SalesLT.ProductModelProductDescription.ProductModelID INNER JOIN SalesLT.ProductDescription ON SalesLT.ProductModelProductDescription.ProductDescriptionID = SalesLT.ProductDescription.ProductDescriptionID WHERE (SalesLT.Product.ProductCategoryID = @filter) AND (SalesLT.ProductModelProductDescription.Culture = N'en')">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <hr />
        <br />
        <asp:Label ID="Label3" runat="server" style="font-weight: 700; font-size: x-large; color: #006600" Text="Izdelki"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GV_Products" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource4" AllowPaging="True" GridLines="None" Height="272px" style="margin-right: 5px" Width="1268px" OnSelectedIndexChanged="GV_Products_SelectedIndexChanged">
            <Columns>
                <asp:TemplateField SortExpression="ThumbnailPhotoFileName">
                <ItemTemplate>
                <asp:Image ID="Image1" runat="server" ImageUrl='<%# "imageHandler.ashx?id=" + Eval("ThumbNailPhotoFileName") %>' />
                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra izdelka" SortExpression="ProductNumber" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="200px" />
                </asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Naziv izdelka" SortExpression="Name" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="200px" />
                </asp:BoundField>
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" >
                <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" >
                <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ListPrice" HeaderText="Cena" SortExpression="ListPrice" DataFormatString="{0:c}" >
                <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:CommandField ShowSelectButton="True" SelectText="Podrobnosti" >
                <ItemStyle Width="100px" />
                </asp:CommandField>
            </Columns>
            <SelectedRowStyle BackColor="#66FF66" BorderColor="#006600" BorderWidth="3px" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductCategoryID, ProductNumber, Name, Size, Color, ListPrice, ProductModelID, ProductID, ThumbnailPhotoFileName FROM SalesLT.Product WHERE (ProductCategoryID = @filter1) AND (ProductModelID = @filter2)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="filter1" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="GV_Models" Name="filter2" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:Label ID="L_Product" runat="server" style="font-weight: 700; font-size: large; color: #006600" Text="Podatki o izdelku"></asp:Label>
        <br />
        <asp:DetailsView ID="DV_Product" runat="server" AutoGenerateRows="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource6" Height="53px" Width="389px" GridLines="None" OnDataBound="DV_Product_DataBound">
            <Fields>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv" SortExpression="Name" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Weight" HeaderText="Teža" SortExpression="Weight" />
                <asp:BoundField DataField="StandardCost" HeaderText="Nabavna cena" SortExpression="StandardCost" DataFormatString="{0:c}" />
                <asp:BoundField DataField="ListPrice" HeaderText="Prodajna cena" SortExpression="ListPrice" DataFormatString="{0:c}" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductNumber, Name, Color, Size, Weight, StandardCost, ListPrice, ProductID FROM SalesLT.Product WHERE (ProductID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:TextBox ID="TB_FilterOrders" runat="server" Visible="False" OnTextChanged="TB_FilterOrders_TextChanged" AutoPostBack="True"></asp:TextBox>
        <br />
        <asp:Label ID="L_Orders" runat="server" style="font-weight: 700; font-size: large; color: #006600" Text="Seznam naročil"></asp:Label>
        <br />
        <asp:GridView ID="GV_Orders" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource5" GridLines="None" Height="119px" style="margin-right: 15px" Width="900px" OnDataBound="GV_Orders_DataBound">
            <Columns>
                <asp:BoundField DataField="SalesOrderID" SortExpression="SalesOrderID" />
                <asp:BoundField DataField="OrderDate" HeaderText="Datum" SortExpression="OrderDate" DataFormatString="{0:d}" >
                <FooterStyle Font-Underline="True" />
                <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Naziv" HeaderText="Kupec" ReadOnly="True" SortExpression="Naziv" >
                <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="OrderQty" HeaderText="Količina" SortExpression="OrderQty" >
                <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="UnitPrice" HeaderText="Cena enote" SortExpression="UnitPrice" DataFormatString="{0:c}" >
                <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="UnitPriceDiscount" HeaderText="Popust" SortExpression="UnitPriceDiscount" DataFormatString="{0:p}" >
                <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="LineTotal" HeaderText="Cena" SortExpression="LineTotal" DataFormatString="{0:c}" ReadOnly="True" >
                <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <br />
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderDetail.ProductID, SalesLT.SalesOrderHeader.OrderDate, SalesLT.SalesOrderDetail.SalesOrderID, SalesLT.Customer.CompanyName + ' (' + SalesLT.Address.City + ', ' + SalesLT.Address.CountryRegion + ')' AS Naziv, SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal FROM SalesLT.SalesOrderDetail INNER JOIN SalesLT.SalesOrderHeader ON SalesLT.SalesOrderDetail.SalesOrderID = SalesLT.SalesOrderHeader.SalesOrderID INNER JOIN SalesLT.Customer ON SalesLT.SalesOrderHeader.CustomerID = SalesLT.Customer.CustomerID INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID WHERE (SalesLT.SalesOrderDetail.ProductID = @filter) AND (SalesLT.Customer.CompanyName LIKE '%' + @filter2+ '%')">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="filter" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="TB_FilterOrders" Name="filter2" PropertyName="Text" DefaultValue="%" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>
