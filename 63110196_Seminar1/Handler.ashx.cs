﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace _63110196_Seminar1
{
    /// <summary>
    /// Summary description for Handler
    /// </summary>
    public class Handler : IHttpHandler
    {

        public void ProcessRequest (HttpContext context) 
        { 
          SqlConnection myConnection = new SqlConnection("ConnectionString"); 
          myConnection.Open();
          string sql = "SELECT ThumbNailPhoto FROM SalesLT.Product WHERE SalesLT.Product.ThumbNailPhotoFileName = @ImageId";
          SqlCommand cmd = new SqlCommand(sql, myConnection);
          cmd.Parameters.Add("@ImageId", SqlDbType.Int).Value = context.Request.QueryString["id"];
          cmd.Prepare();
          SqlDataReader dr = cmd.ExecuteReader();
          dr.Read();
          context.Response.ContentType = "image/bmp";
          context.Response.BinaryWrite((byte[])dr["ThumbNailPhoto"]);
          dr.Close();
          myConnection.Close();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}