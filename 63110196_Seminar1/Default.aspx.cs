﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63110196_Seminar1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

       
       
        protected void GV_Orders_DataBound(object sender, EventArgs e)
        {
            if (GV_Orders.Rows.Count <= 0)
            {
                L_Orders.Visible = false;
                TB_FilterOrders.Visible = false;

            }
            else
            {
                L_Orders.Visible = true;
                TB_FilterOrders.Visible = true;

            }
        }
        protected void TB_FilterOrders_TextChanged(object sender, EventArgs e)
        {
            Page_Load(null, EventArgs.Empty);
        }
        protected void GV_Products_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void DV_Product_DataBound(object sender, EventArgs e)
        {
            if (DV_Product.Rows.Count <= 0)
            {
                L_Product.Visible = false;
            }
            else
            {
                L_Product.Visible = true;
            }
        }

        

             
    }
}